import React from 'react';
import {createStyles, makeStyles, Theme} from '@material-ui/core/styles';
import {Grid} from "@material-ui/core";
import WelcomeText from "../components/WelcomeText";
import LoginCard from "../components/LoginCard";


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            //stylings :)
            justifyContent: 'center',
            alignItems: 'center'
        },
    }),
);

export default function WelcomeImageArea() {
    const classes = useStyles();


    return (
        <Grid container className={classes.root}>
            <WelcomeText/>
            <LoginCard/>
        </Grid>
    );
}
