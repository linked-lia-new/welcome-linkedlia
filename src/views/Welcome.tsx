import React from 'react';
import {createStyles, makeStyles, Theme} from '@material-ui/core/styles';
import WelcomeImageArea from "./WelcomeImageArea";
import LogoPlaceholder from "../components/LogoPlaceHolder";


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            //stylings :)
        }
    }),
);


export default function Welcome() {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <header className="Profile-header">
                <div style={{height: '12vh'}}>
                    <LogoPlaceholder/>
                </div>
            </header>
            <div style={{marginTop: '64px'}}>
                <WelcomeImageArea/>
            </div>
        </div>
    );
}
