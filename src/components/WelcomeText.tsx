import React, {useEffect, useState} from 'react';
import {createStyles, makeStyles, Theme} from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import Typography from '@material-ui/core/Typography';
import {Grid} from "@material-ui/core";
import back1 from "../assets/back1.png";
import CardContent from "@material-ui/core/CardContent";



const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        imageArea: {
            backgroundImage: `url(${back1})`,
            marginLeft: 40,
            marginRight: 40,
            marginBottom: 40
        },
        textArea: {
            width: "70%",
            marginTop: "10%",
            marginRight: "10%",
            marginLeft: "5%",
            marginBottom: "20%"
        },
        pos: {
            marginBottom: 12,
        },
    }),
);

export default function WelcomeText() {
    const classes = useStyles();

    return (
            <Grid item lg={6} md={6} sm={12} xs={12}>
                <Card className={classes.imageArea} variant="outlined">
                    <CardContent>
                        <Card className={classes.textArea} variant="outlined">
                            <CardContent>
                                <Typography variant="h5" component="h2">
                                    Linked-LIA
                                </Typography>
                                <Typography className={classes.pos} color="textSecondary">
                                    Vi för samman studenter och företag för att skapa nya möjligheter.
                                </Typography>
                                <Typography variant="body2" component="p">
                                    Linked-LIA är plattformen som för samman studenter som söker praktik med företag som
                                    söker framtida personal.
                                    <br/>
                                    <br/>
                                    <Typography variant="body2" component="p">
                                        Är du student och letar praktikplats? Eller representerar du ett företag på jakt efter ny kompetens?
                                        Då är detta stället för dig. Här kan du enkelt presentera dig/ditt företag och samtidigt leta
                                        efter det just du söker.
                                        Kontakta gärna oss på Linked-LIA om du har några frågor eller behöver vår hjälp.
                                    </Typography>
                                </Typography>
                            </CardContent>
                        </Card>
                    </CardContent>
                </Card>
            </Grid>
    );
}
