import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import {makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
    paper: {
        backgroundColor: "#eeedec"
    }

}));

export default function PopUpWindow(props: any) {
    const classes = useStyles();
    const {title, children, openWindow, closeWindow} = props;


    return (
        <div>
            <Dialog
                open={openWindow}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
                PaperProps={{
                    classes: {
                        root: classes.paper
                    }
                }}
            >
                <DialogTitle style={{backgroundColor: "#a9b7c0", color: "#eeedec"}} id="alert-dialog-title">Fyll i dina
                    uppgifter {title}
                </DialogTitle>
                <DialogContent>
                    {children}
                </DialogContent>
                <DialogActions style={{backgroundColor: "#a9b7c0"}}>
                    <Button onClick={closeWindow} style={{color: "#eeedec"}}>
                        Stäng
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}
