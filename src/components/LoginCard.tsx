import React, {useEffect, useState} from 'react';
import {createStyles, makeStyles, Theme} from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import {Grid} from "@material-ui/core";
import {useKeycloak} from "@react-keycloak/web";
import PopUpWindow from "../components/PopUpWindow";
import CreateUserForm from "../components/CreateUserForm";


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            //stylings :)
            justifyContent: 'center',
            alignItems: 'center'
        },
        loginArea: {
            margin: 30,
            backgroundColor: '#a9b7c0'
        },
        title: {
            fontSize: 14,
        },
        pos: {
            marginBottom: 12,
        },
    }),
);

export default function LoginCard() {
    const classes = useStyles();
    const [openWindow, setOpenWindow] = React.useState(false);
    const closeWindow = () => {
        setOpenWindow(false);
    }

    const {keycloak, initialized} = useKeycloak();
    const kcToken = keycloak?.token ?? '';
    const [users, setUsers] = useState<string[]>([])

    useEffect(() => {
            if (kcToken) {
                fetch("http://localhost:8088/students", {
                    headers: {
                        "Authorization": "Bearer " + kcToken
                    }
                })
                    .then(res => res.json())
                    .then(students => setUsers(students))
                    .catch(reason => console.error("Failed to fetch ", reason))
            }
        },
        [keycloak.authenticated, kcToken])


    useEffect(() => {
            setInterval(() => {
                if (keycloak.authenticated) {
                    if (keycloak.hasRealmRole("user"))
                        window.location.href = "http://localhost:3001";
                    else if (keycloak.hasRealmRole("company"))
                        window.location.href = "http://localhost:3002";
                    else
                        console.log("keycloak role ")
                }
            }, 1000);
        },

        [keycloak.authenticated, kcToken]);

    return (
        <Grid item lg={3} md={4} sm={8} xs={10}>
            <Card className={classes.loginArea} variant="outlined">
                <CardContent>
                    <CardActions style={{justifyContent: "center"}}>
                        {
                            keycloak.authenticated ?
                                <Button
                                    type="button"
                                    onClick={() => keycloak.logout()}>
                                    logga ut
                                </Button>
                                :
                                <Button style={{justifyContent: "center"}}
                                        type="button"
                                        onClick={() => keycloak?.login()}>
                                    logga in
                                </Button>
                        }
                    </CardActions>
                    <CardActions style={{justifyContent: "center"}}>
                        <Button
                            type="button"
                            onClick={() => setOpenWindow(true)}>
                            skapa konto
                        </Button>
                    </CardActions>
                    <PopUpWindow openWindow={openWindow} closeWindow={closeWindow}>
                        <CreateUserForm/>
                    </PopUpWindow>
                </CardContent>
            </Card>
        </Grid>
    );
}
