import React, {ChangeEvent, useState} from 'react';
import {createStyles, makeStyles, Theme} from "@material-ui/core/styles";
import {Paper, TextField} from "@material-ui/core";
import Button from '@material-ui/core/Button';
import {useForm} from "react-hook-form";
import HandleApi from "../api/HandleApi";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            //flexGrow: 1,
            maxHeight: "80vh",
            width: "80%",
            height: "80%",
            margin: "20px",
            background: "#a9b7c0",
            justifyContent: "center"

        },
        form: {
            '& .MuiTextField-root': {
                margin: theme.spacing(1),
                justifyContent: "center",
                padding: "1%"
            },
        },
        formdesign: {
            display: "flex",
            justifyContent: "around",
            color: "#eeedec"
        },
        container: {
            textAlign: "right"
        },
    }),
);

type FormData = {
    username: string;
    firstName: string;
    lastName: string;
    password: string;
    email: string;
};

export default function CreateUserForm() {
    const classes = useStyles();
    const {register, setValue, handleSubmit, formState: {errors}} = useForm<FormData>();

    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [password, setPassword] = useState("");
    const [username, setUsername] = useState("");
    const [email, setEmail] = useState("");


    const handleUsernameChange = (event: ChangeEvent<HTMLInputElement>) => {
        let username = event.target.value;
        setUsername(username);
    }
    const handleFirstNameChange = (event: ChangeEvent<HTMLInputElement>) => {
        let newName = event.target.value;
        setFirstName(newName);
    }
    const handleLastNameChange = (event: ChangeEvent<HTMLInputElement>) => {
        let newName = event.target.value;
        setLastName(newName);
    }
    const handlePasswordChange = (event: ChangeEvent<HTMLInputElement>) => {
        let password = event.target.value;
        setPassword(password);
    }
    const handleEmailChange = (event: ChangeEvent<HTMLInputElement>) => {
        let newemail = event.target.value;
        setEmail(newemail);
    }


    const onSubmit = handleSubmit((data) => {
        HandleApi.createUser({
            username: data.username,
            firstName: data.firstName,
            lastName: data.lastName,
            password: data.password,
            email: data.email
        })
            .then((res: any) => console.log(res))
            .catch((reason: any) => alert(reason))
    });


    return (
        <>
            <Paper elevation={20} className={classes.root}>
                <form className={classes.form} noValidate autoComplete="off" onSubmit={onSubmit}>
                    <div className={classes.formdesign}>
                        <TextField {...register("username")}
                                   size="small"
                                   id="outlined-name-input"
                                   label="Användarnamn"
                                   value={username}
                                   variant="outlined"
                                   style={{width: "80%"}}
                                   required
                                   onChange={handleUsernameChange}
                        />
                    </div>
                    <div className={classes.formdesign}>
                        <TextField {...register("firstName")}
                                   size="small"
                                   id="outlined-name-input"
                                   label="Förnamn"
                                   value={firstName}
                                   variant="outlined"
                                   style={{width: "80%"}}
                                   required
                                   onChange={handleFirstNameChange}

                        />
                    </div>
                    <div className={classes.formdesign}>
                        <TextField {...register("lastName")}
                                   size="small"
                                   id="outlined-lastname-input"
                                   label="Efternamn"
                                   value={lastName}
                                   variant="outlined"
                                   style={{width: "80%"}}
                                   required
                                   onChange={handleLastNameChange}

                        />
                    </div>

                    <div className={classes.formdesign}>
                        <TextField {...register("email")}
                                   size="small"
                                   id="outlined-email-input"
                                   label="E-mail"
                                   value={email}
                                   variant="outlined"
                                   style={{width: "80%"}}
                                   required
                                   onChange={handleEmailChange}

                        />
                    </div>
                    <div className={classes.formdesign}>
                        <TextField {...register("password")}
                                   size="small"
                                   id="outlined-email-input"
                                   label="Lösernord"
                                   value={password}
                                   variant="outlined"
                                   style={{width: "80%"}}
                                   required
                                   onChange={handlePasswordChange}
                        />
                    </div>
                    <div className={classes.container}>
                        <Button type="submit" style={{color: "#eeedec", marginTop: "10px"}}>
                            SPARA
                        </Button>
                    </div>
                </form>
            </Paper>
        </>
    )
}
