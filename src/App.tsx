import React from 'react';
import Welcome from "./views/Welcome";
import {HashRouter as Router} from 'react-router-dom';
import keycloak from "./keycloak";
import {ReactKeycloakProvider} from '@react-keycloak/web';


function App() {
    return (
        <ReactKeycloakProvider authClient={keycloak}>
            <Router>
                <Welcome/>
            </Router>
        </ReactKeycloakProvider>
    );
}

export default App;
