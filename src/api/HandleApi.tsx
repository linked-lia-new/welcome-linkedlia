import React from 'react'
import Keycloak from "keycloak-js";
import keycloak from "../keycloak";

export default class HandleApi {

    public static createUser = async (data = {}) => {
        const response = await fetch("http://localhost:8088/students/create_student",
            {
                method: 'POST',
                headers: {"Content-type": "application/json"},
                body: JSON.stringify(data)
            });
        return await response.json();
    }

    // static createUser2(kcToken: any, data1: { firstName: any; lastName: any; password: any; email: any; username: any }) {
    //
    // }
}